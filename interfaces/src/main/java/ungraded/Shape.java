package ungraded;

public interface Shape {
    double getArea();
    double getPerimeter();
}