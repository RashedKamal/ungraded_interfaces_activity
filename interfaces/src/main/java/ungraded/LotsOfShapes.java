package ungraded;

public class LotsOfShapes {
    public static void main (String[] args){
        Shape[] shapes = new Shape[5];
        shapes[0] = new Rectangle(1, 2);
        shapes[1] = new Rectangle(2, 3);
        shapes[2] = new Circle(4);
        shapes[3] = new Circle(5);
        shapes[4] = new Square(6);

        for (Shape shape : shapes) {
            System.out.println("Area: " + shape.getArea());
            System.out.println("Perimeter: " + shape.getPerimeter());
        }
    }
}
